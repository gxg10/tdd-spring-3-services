package com.brande.reviewservice;

import com.brande.reviewservice.model.Review;
import com.brande.reviewservice.model.ReviewEntry;
import com.brande.reviewservice.repository.ReviewRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.io.File;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@DataMongoTest
public class ReviewRepositoryTest {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private ReviewRepository repository;

    private ObjectMapper mapper = new ObjectMapper();

    private static File SAMPLE_JSOn = Paths.get("src", "test",
            "resources", "data", "sample.json").toFile();

    @BeforeEach
    void beforeEach() throws Exception {
        Review[] objects = mapper.readValue(SAMPLE_JSOn, Review[].class);

        Arrays.stream(objects).forEach(mongoTemplate::save);
    }

    @AfterEach
    void afterEach() {
        mongoTemplate.dropCollection("Reviews");
    }

    @Test
    void testFindAll() {
        List<Review> reviews = repository.findAll();
        Assertions.assertEquals(2, reviews.size(), "should be twp" +
                "reviews in the db");
    }

    @Test
    void testFindByIdSuccess() {
        Optional<Review> review = repository.findById("1");
        Assertions.assertTrue(review.isPresent(), "we shoul dhave found a " +
                "review with id 1");
        review.ifPresent(r -> {
            Assertions.assertEquals("1", r.getId(), "review id " +
                    "should be 1");
            Assertions.assertEquals(1, r.getProductId().intValue(), "review " +
                    "product id should be 1");
            Assertions.assertEquals(1, r.getVersion().intValue(), "rev version" +
                    "should be 1");
            Assertions.assertEquals(1, r.getEntries().size(), "review1 should " +
                    "have only one entry");
        });
    }
    @Test
    void testFindBIdFailure() {
        Optional<Review> review = repository.findById("99");
        Assertions.assertFalse(review.isPresent(), "we hosuld not " +
                "find a review with id 99");
    }

    @Test
    void testFindByIdProdcutSuccess() {
        Optional<Review> review = repository.findByProductId(1);
        Assertions.assertTrue(review.isPresent(), "there should be a review wth" +
                "product id 1");
    }

    @Test
    void testFindByProductIdFailure() {
        Optional<Review> review = repository.findByProductId(99);
        Assertions.assertFalse(review.isPresent(), "There should not be a review for product ID 99");
    }

    @Test
    void testSave() {
        Review review = new Review(10, 1);
        review.getEntries().add(new ReviewEntry("test-user", new Date(),
                "this is a review"));

        Review savedReview = repository.save(review);

        Optional<Review> loadedReview = repository.findById(savedReview.getId());

        Assertions.assertTrue(loadedReview.isPresent());

        loadedReview.ifPresent(r -> {
            Assertions.assertEquals(10, r.getProductId().intValue());
            Assertions.assertEquals(1, r.getVersion().intValue(),
                    "reveiw version should be 1");
            Assertions.assertEquals(1, r.getEntries().size(), "" +
                    "review 1 should have 1 entry");
        });
    }

    @Test
    void testUpdate() {
        // Retrieve review 2
        Optional<Review> review = repository.findById("2");
        Assertions.assertTrue(review.isPresent(), "Review 2 should be present");
        Assertions.assertEquals(3, review.get().getEntries().size(), "There should be 3 review entries");

        // Add an entry to the review and save
        Review reviewToUpdate = review.get();
        reviewToUpdate.getEntries().add(new ReviewEntry("test-user-2", new Date(), "This is a fourth review"));
        repository.save(reviewToUpdate);

        // Retrieve the review again and validate that it now has 4 entries
        Optional<Review> updatedReview = repository.findById("2");
        Assertions.assertTrue(updatedReview.isPresent(), "Review 2 should be present");
        Assertions.assertEquals(4, updatedReview.get().getEntries().size(), "There should be 3 review entries");
    }

    @Test
    void testDelete() {
        // Delete review 2
        repository.deleteById("2");

        // Confirm that it is no longer in the database
        Optional<Review> review = repository.findById("2");
        Assertions.assertFalse(review.isPresent(), "Review 2 should now be deleted from the database");
    }

}
