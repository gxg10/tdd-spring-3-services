package com.brande.reviewservice;

import com.brande.reviewservice.model.Review;
import com.brande.reviewservice.model.ReviewEntry;
import com.brande.reviewservice.repository.ReviewRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@DataMongoTest
@ExtendWith(MongoSpringExtension.class)
public class ReviewRepositoryTestMoreElegant {

    @Autowired
    private ReviewRepository repository;

    @Autowired
    private MongoTemplate mongoTemplate;

    public MongoTemplate getMongoTemplate() {
        return mongoTemplate;
    }

    @Test
    @MongoDataFile(value = "sample.json",classType = Review.class, collectionName = "Reviews")
    void testSave() {
        Review review = new Review(10, 1);
        review.getEntries().add(new ReviewEntry("test-user", new Date(),
                "This is a review"));

        Review savedReview = repository.save(review);

        Optional<Review> loadedReview = repository.findById(savedReview.getId());

        Assertions.assertTrue(loadedReview.isPresent());

        loadedReview.ifPresent(r -> {
            Assertions.assertEquals(10, r.getProductId().intValue());
            Assertions.assertEquals(1, r.getVersion().intValue(), "Review vers " +
                    "should be 1");
            Assertions.assertEquals(1, r.getEntries().size(), "review 1 should have one entry");
        });
    }

    @Test
    @MongoDataFile(value = "sample6.json", classType = Review.class, collectionName = "Reviews")
    void testFindAll6() {
        List<Review> reviews = repository.findAll();
        Assertions.assertEquals(6, reviews.size(), "should be 6");
        reviews.forEach(System.out::println);
    }
}
