package com.brande.inventoryservice;

import com.brande.inventoryservice.model.InventoryRecord;
import com.brande.inventoryservice.service.InventoryService;
import com.github.tomakehurst.wiremock.WireMockServer;
import static com.github.tomakehurst.wiremock.client.WireMock.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import java.util.Optional;

@SpringBootTest
@TestPropertySource(locations = "classpath:test.properties")
public class InventoryServiceTest {

    @Autowired
    private InventoryService service;

    private WireMockServer server;

    @BeforeEach
    void beforeEach() {
        server = new WireMockServer(9999);
        server.start();

        server.stubFor(get(urlEqualTo("/inventory/1"))
        .willReturn(aResponse()
        .withHeader("Content-Type", "application/json")
        .withStatus(200)
        .withBodyFile("json/inventory-response.json")));

        server.stubFor(get(urlEqualTo("/inventory/2"))
        .willReturn(aResponse().withStatus(404)));

        server.stubFor(post("/inventory/1/purchaseRecord")
        .withHeader("Content-Type", containing("application/json"))
        .withRequestBody(containing("\"productId\":1"))
        .willReturn(aResponse()
        .withHeader("Content-Type", "application/json")
        .withStatus(200)
        .withBodyFile("json/inventory-response-after-post.json")));


    }

    @AfterEach
    void afterEach() {
        server.stop();
    }

    @Test
    void testGetInventoryRecordSuccess() {
        Optional<InventoryRecord> record = service.getInventoryRecord(1);
        Assertions.assertTrue(record.isPresent(), "inventory rec should be" +
                "present");
        Assertions.assertEquals(500, record.get().getQuantity().intValue(),
                "the quantity should be 500");
    }

    @Test
    void testGetInventoryNotFound() {
        Optional<InventoryRecord> record = service.getInventoryRecord(2);
        Assertions.assertFalse(record.isPresent(), "inv should not be present");
    }

    @Test
    void testPurchaseProductSuccess() {
        Optional<InventoryRecord> record = service.purchaseProduct(1, 1);
        Assertions.assertTrue(record.isPresent(), "inv should be present");

        Assertions.assertEquals(499, record.get().getQuantity().intValue(),
                "the q should b e499");
    }
}
