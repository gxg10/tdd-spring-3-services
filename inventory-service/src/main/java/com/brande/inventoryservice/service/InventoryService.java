package com.brande.inventoryservice.service;

import com.brande.inventoryservice.model.InventoryRecord;

import java.util.Optional;

public interface InventoryService {
    Optional<InventoryRecord> getInventoryRecord(Integer productId);
    Optional<InventoryRecord> purchaseProduct(Integer productId,
                                              Integer quantity);
}
