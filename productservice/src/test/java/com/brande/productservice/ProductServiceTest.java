package com.brande.productservice;

import com.brande.productservice.model.Product;
import com.brande.productservice.repository.ProductRepository;
import com.brande.productservice.service.ProductService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class ProductServiceTest {

    @Autowired
    private ProductService service;

    @MockBean
    private ProductRepository repository;

    @Test
    @DisplayName("Test findById Success")
    void testFindByIdSuccess() {
        Product mockProduct = new Product(1, "Product Name", 10, 11);
        doReturn(Optional.of(mockProduct)).when(repository).findById(1);

        Optional<Product> returnedProduct = service.findById(1);

        Assertions.assertTrue(returnedProduct.isPresent(), "Product was not found");
        Assertions.assertSame(returnedProduct.get(), mockProduct, "Products shou " +
                "uld be the same");
    }

    @Test
    @DisplayName("test findbid not found")
    void testFindByIdNotFound() {
        Product mockProduct = new Product(1, "Product Name", 10, 1);
        doReturn(Optional.empty()).when(repository).findById(1);

        Optional<Product> returnedProduct = service.findById(1);

        Assertions.assertFalse(returnedProduct.isPresent(), "product was found, " +
                "whe it shouldnt be");
    }

    @Test
    @DisplayName("test finda all")
    void testFindAll() {
        Product mockProduct = new Product(1, "Product Name", 10, 1);
        Product mockProduct2 = new Product(2, "Product Name 2", 15, 3);
        doReturn(Arrays.asList(mockProduct, mockProduct2)).when(repository).findAll();

        List<Product> products = service.findAll();

        Assertions.assertEquals(2, products.size(), "find all sould" +
                "return 2 producs");
    }

    @Test
    @DisplayName("tst saveproduct")
    void testSave() {
        Product mockProduct = new Product(1, "Product Name", 10);
        doReturn(mockProduct).when(repository).save(any());

        Product returnedProduct = service.save(mockProduct);

        Assertions.assertNotNull(returnedProduct, "the saved product should " +
                "not be null)");
        Assertions.assertEquals(1, returnedProduct.getVersion().intValue(),
                "the version for anew produdct should be 1");
    }
}
