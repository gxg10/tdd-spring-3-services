package com.brande.productservice;

import com.brande.productservice.model.Product;
import com.brande.productservice.repository.ProductRepository;
import com.github.database.rider.core.api.connection.ConnectionHolder;
import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.junit5.DBUnitExtension;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.sql.DataSource;
import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@ExtendWith({DBUnitExtension.class, SpringExtension.class})
@SpringBootTest
@ActiveProfiles("test")
public class ProductRepositoryTest {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private ProductRepository repository;

    public ConnectionHolder getConnectionHolder() {
        return () -> dataSource.getConnection();
    }

    @Test
    @DataSet("products.yml")
    void testFindAll() {
        List<Product> products = repository.findAll();
        Assertions.assertEquals(2, products.size(), "" +
                "we should have 2 products in our db");
    }

    @Test
    @DataSet("products.yml")
    void testFindByIdSuccess() {
        Optional<Product> product = repository.findById(2);

        Assertions.assertTrue(product.isPresent(), "product wiht id 2 ," +
                "should be found");

        Product p = product.get();

        Assertions.assertEquals(2, p.getId().intValue(),
                "pr id should be 2");
        Assertions.assertEquals("Product 2", p.getName(), "pr nam" +
                "should be product 2");
        Assertions.assertEquals(5, p.getQuantity().intValue(), "pr q" +
                "should be 5");
        Assertions.assertEquals(2, p.getVersion().intValue(),
                "pr version should be 2");
    }

    @Test
    @DataSet("products.yml")
    void testFindByIdNotFound() {
        Optional<Product> product = repository.findById(3);

        Assertions.assertFalse(product.isPresent(), "pr with id 3 " +
                "should not be found");
    }

    @Test
    @DataSet(value = "products.yml")
    void testSave() {
        Product product = new Product("Product 5", 5);
        product.setVersion(1);

        Product savedProduct = repository.save(product);

        Assertions.assertEquals("Product 5", savedProduct.getName());
        Assertions.assertEquals(5, savedProduct.getQuantity().intValue());

        Optional<Product> loaddedProduct = repository.findById(savedProduct.getId());
        Assertions.assertTrue(loaddedProduct.isPresent(), "culd not reload product from db");
        Assertions.assertEquals("Product 5", loaddedProduct.get().getName(), "pr name does not match");
        Assertions.assertEquals(5, loaddedProduct.get().getQuantity().intValue(), "" +
                "pr quantiy does not mtacth");
        Assertions.assertEquals(1, loaddedProduct.get().getVersion().intValue(), "" +
                "ver" +
                "does not match");
    }

    @Test
    @DataSet(value = "products.yml")
    void testUpdateSuccess() {
        Product product = new Product(1, "This is product 1", 100, 5);
        boolean res = repository.update(product);

        Assertions.assertTrue(res, "the product shoul be updated");

        Optional<Product> loadedProduct = repository.findById(1);
        Assertions.assertTrue(loadedProduct.isPresent(), "updadetd " +
                "product shoul exist in the db");
        Assertions.assertEquals("This is product 1", loadedProduct.get().getName(),
                "the product s name should be the same");
        Assertions.assertEquals(100, loadedProduct.get().getQuantity().intValue(),
                "the q should have been the same");
        Assertions.assertEquals(5, loadedProduct.get().getVersion().intValue(),
                "the version should be the same");
    }

    @Test
    @DataSet(value = "products.yml")
    void testUpdateFailure() {
        Product product = new Product(3, "this is product 3", 100, 5);
        boolean res = repository.update(product);

        Assertions.assertFalse(res, "the product should not have been " +
                "updated");
    }

    @Test
    @DataSet("products.yml")
    void testDeleteSuccess() {
        boolean res = repository.delete(1);
        Assertions.assertTrue(res, "delete should return true on succes");

        Optional<Product> product = repository.findById(1);
        Assertions.assertFalse(product.isPresent(), "pro with " +
                "id 1 should have been deleted");
    }

    @Test
    @DataSet("products.yml")
    void testDeleteFailure() {
        boolean result = repository.delete(3);
        System.out.println("res " +result);
        Assertions.assertFalse(result, "delete should return false" +
                "because the deleteion failed");
    }
}
