create table if not exists products (
    id integer not null auto_increment,
    name varchar(128) not null,
    quantity integer not null,
    version integer not null,
    primary key (id)
);