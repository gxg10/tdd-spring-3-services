package com.brande.productservice.service;

import com.brande.productservice.model.Product;

import java.util.List;
import java.util.Optional;

public interface ProductService {

    Optional<Product> findById(Integer id);

    List<Product> findAll();

    boolean update(Product product);

    Product save(Product product);

    boolean delete(Integer id);
}
